//
//  BaseCoordinator.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import UIKit

/**
 BaseCoordinator protocol defines the most basic functionality of a coordinator.
 However, it doesnt handle dismiss of the child coordinator.
 It is recommended to use this protocol to create a coordinator that doesnt dismiss like AppCoordinator or root coordinator of a flow like ContactList
 */
public protocol BaseCoordinator: class {
    init(services: Services, presenter: UINavigationController)
    func start()
    var identifier: UUID { get }
    var childCoordinators: [UUID: BaseCoordinator] { get set }

    var finish: (_ coordinator: BaseCoordinator) -> Void { get set }
    var presenter: UINavigationController { get }
    var services: Services { get }

    func addChild(_ coordinator: BaseCoordinator)
    func removeChild(_ coordinator: BaseCoordinator)
}

extension BaseCoordinator {
    public func addChild(_ coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = coordinator
    }

    public func removeChild(_ coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = nil
    }
}
