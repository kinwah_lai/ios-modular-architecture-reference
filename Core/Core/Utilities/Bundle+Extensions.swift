//
//  Bundle+Extensions.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Foundation

extension Bundle {
    public var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }

    public var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }

    public var appId: String? {
        return infoDictionary?["CFBundleIdentifier"] as? String
    }
}
