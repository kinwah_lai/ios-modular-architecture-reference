//
//  String+LastPathComponent.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Foundation

extension String {
    public var fileURL: URL {
        return URL(fileURLWithPath: self)
    }

    public var lastPathComponent: String {
        get {
            return fileURL.lastPathComponent
        }
    }
}
