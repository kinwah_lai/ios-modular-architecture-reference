//
//  Logger.swift
//  Core
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Foundation
import SwiftyBeaver

public var log: SwiftyBeaver.Type {
    return SwiftyBeaver.self
}
