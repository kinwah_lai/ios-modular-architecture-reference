//
//  InputOutputViewModelType.swift
//  Core
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Foundation

// learned InputOutput style from
// https://benoitpasquier.com/rxswift-mvvm-alternative-structure-for-viewmodel/
// https://stackoverflow.com/questions/54163088/rxswift-mvvm-validate-form-on-button-submit-then-make-api-request
public protocol InputOutputViewModelType {
    associatedtype Input
    associatedtype Output

    var input: Input { get }
    var output: Output { get }
}
