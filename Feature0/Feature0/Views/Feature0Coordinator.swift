//
//  Feature0Coordinator.swift
//  Feature0
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import Foundation

final public class Feature0Coordinator: BaseCoordinator {
    private (set) public var identifier: UUID = UUID()
    public var childCoordinators: [UUID: BaseCoordinator] = [:]
    public var finish: (BaseCoordinator) -> Void = { _ in }
    public let presenter: UINavigationController
    public let services: Services

    public init(services: Services, presenter: UINavigationController) {
        self.presenter = presenter
        self.services = services
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    public func start() {
        let viewModel = Feature0ViewModel()
        // NOTE: we need to use the right bundle for the view controller
        let feature = Feature0ViewController(nibName: nil, bundle: Bundle(for: type(of: self)))
        let nav = UINavigationController(rootViewController: feature)
        feature.delegate = self
        feature.viewModel = viewModel
        presenter.present(nav, animated: true, completion: nil)
    }
}

extension Feature0Coordinator: Feature0ViewControllerDelegate {
    func wantToCloseModal(at vc: Feature0ViewController) {
        vc.dismiss(animated: true, completion: nil)
        finish(self)
    }
}
