//
//  Feature0ViewModel.swift
//  Feature0
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import Foundation
import RxSwift

final class Feature0ViewModel: InputOutputViewModelType {
    enum AccessibilityIdentifier: String {
        case featureLabel = "feature-label"
    }

    let input: Feature0ViewModel.Input
    let output: Feature0ViewModel.Output

    struct Input {}

    struct Output {
        let featureLabelObservable: Observable<String>
        let backgroundColorObservable: Observable<UIColor>
    }

    init() {
        input = Input()
        let featureLabel = Observable.just("This is feature 0 presented as modal")
        let bgColor = Observable.just(UIColor.lightGray)
        output = Output(featureLabelObservable: featureLabel,
                        backgroundColorObservable: bgColor)
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }
}
