//
//  Feature0ViewController.swift
//  Feature0
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import RxCocoa
import RxSwift
import UIKit

protocol Feature0ViewControllerDelegate: class {
    func wantToCloseModal(at: Feature0ViewController)
}

class Feature0ViewController: UIViewController {
    weak var delegate: Feature0ViewControllerDelegate?
    var disposeBag: DisposeBag = DisposeBag()
    var viewModel: Feature0ViewModel!

    @IBOutlet weak var featureLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAccessibilityIdentifier()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
        navigationItem.rightBarButtonItem = doneButton
        disposeBag.insert(
            doneButton.rx.tap.subscribe(onNext: { [unowned self] _ in
                self.delegate?.wantToCloseModal(at: self)
            }),
            viewModel.output.featureLabelObservable.bind(to: featureLabel.rx.text),
            viewModel.output.backgroundColorObservable.bind(to: view.rx.backgroundColor)
        )
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    private func setupAccessibilityIdentifier() {
        featureLabel.accessibilityIdentifier = Feature0ViewModel.AccessibilityIdentifier.featureLabel.rawValue
    }
}
