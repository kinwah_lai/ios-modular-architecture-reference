//
//  CodableResponses.swift
//  Contacts
//
//  Created by Darren Lai on 5/21/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import Foundation

struct CodableResponse: Decodable {
    var info: Info
    var results: [Contact.DTO]
    enum CodingKeys: String, CodingKey {
        case info
        case results
    }
}

struct Info: Decodable {
    var seed: String
    var results: Int
    var page: Int

    enum CodingKeys: String, CodingKey {
        case seed
        case results
        case page
    }
}
