//
//  Address.swift
//  Contacts
//
//  Created by Darren Lai on 5/23/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import CoreData
import Foundation

@objc(Address)
final class Address: NSManagedObject {
    @NSManaged public var street: String
    @NSManaged public var city: String
    @NSManaged public var state: String
    @NSManaged public var postcode: Int
    @NSManaged public var contact: Contact?

    convenience init(with dto: DTO, in context: NSManagedObjectContext) throws {
        self.init(context: context)
        update(from: dto)
    }

    func update(from dto: DTO) {
        street = dto.street
        city = dto.city
        state = dto.state
        postcode = dto.postcode
    }
}

extension Address: DecodableDTO {
    struct DTO: Decodable {
        fileprivate enum CodingKeys: String, CodingKey {
            case street
            case city
            case state
            case postcode
        }
        let street: String
        let city: String
        let state: String
        let postcode: Int

        init(street: String, city: String, state: String, postcode: Int) {
            self.street = street
            self.city = city
            self.state = state
            self.postcode = postcode
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            street = try container.decode(String.self, forKey: .street)
            city = try container.decode(String.self, forKey: .city)
            state = try container.decode(String.self, forKey: .state)
            postcode = try container.decode(Int.self, forKey: .postcode)
        }
    }
}

extension Address {
    @nonobjc public class func fetchRequest(predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor] = []) -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address", predicate: predicate, sortDescriptors: sortDescriptors)
    }
}
