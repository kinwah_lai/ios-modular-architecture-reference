//
//  Contact.swift
//  Contacts
//
//  Created by Darren Lai on 5/20/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import CoreData
import Foundation

protocol DecodableDTO {
    associatedtype DTO: Decodable
}

@objc(Contact)
final class Contact: NSManagedObject, Managed {
    @NSManaged public var firstname: String
    @NSManaged public var lastname: String
    @NSManaged public var email: String
    @NSManaged public var phone: String?
    @NSManaged public var gender: String?
    @NSManaged public var pictureLarge: String?
    @NSManaged public var pictureMedium: String?
    @NSManaged public var pictureThumbnail: String?
    @NSManaged public var address: Address?

    var pictureLargeUrl: URL? {
        guard let picture = pictureLarge else { return nil }
        return URL(string: picture)
    }
    var pictureMediumUrl: URL? {
        guard let picture = pictureMedium else { return nil }
        return URL(string: picture)
    }
    var pictureThumbnailUrl: URL? {
        guard let picture = pictureThumbnail else { return nil }
        return URL(string: picture)
    }

    convenience init(with dto: DTO, in context: NSManagedObjectContext) throws {
        self.init(context: context)
        update(from: dto)
    }

    class func findOrCreate(for dto: DTO, in context: NSManagedObjectContext) throws -> Contact {
        let identifablePredicate = NSPredicate(format: "%K == %@", "email", dto.email)
        let object = try findOrCreate(in: context, matching: identifablePredicate)
        object.update(from: dto)
        return object
    }

    func update(from dto: DTO) {
        firstname = dto.firstName
        lastname = dto.lastName
        email = dto.email
        phone = dto.phone
        gender = dto.gender
        pictureLarge = dto.pictureLarge
        pictureMedium = dto.pictureMedium
        pictureThumbnail = dto.pictureThumbnail
        if let address = address {
            // update
            if let dtoAddress = dto.address {
                address.update(from: dtoAddress)
                address.contact = self
            } else {
                self.address = nil
            }
        } else {
            if let dtoAddress = dto.address, let context = managedObjectContext {
                address = try? Address(with: dtoAddress, in: context)
                address?.contact = self
            }
        }
    }
}

extension Contact: DecodableDTO {
    struct DTO: Decodable {
        let firstName: String
        let lastName: String
        let email: String
        let phone: String?
        let gender: String?
        var address: Address.DTO?
        var pictureLarge: String?
        var pictureMedium: String?
        var pictureThumbnail: String?

        fileprivate enum PictureCodingKeys: String, CodingKey {
            case large
            case medium
            case thumbnail
        }

        fileprivate enum NameCodingKeys: String, CodingKey {
            case firstName = "first"
            case lastName = "last"
        }

        fileprivate enum CodingKeys: String, CodingKey {
            case name
            case gender
            case email
            case phone
            case picture
            case address = "location"
        }

        private static func hasherForAddress(_ email: String, _ firstName: String, _ lastName: String) -> Int {
            var hasher = Hasher()
            hasher.combine(email)
            hasher.combine(firstName)
            hasher.combine(lastName)
            return hasher.finalize()
        }

        init(firstname: String, lastname: String, email: String, phone: String?, gender: String?, address: Address.DTO?) {
            firstName = firstname
            lastName = lastname
            self.email = email
            self.phone = phone
            self.gender = gender
            self.address = address
            pictureThumbnail = nil
            pictureMedium = nil
            pictureLarge = nil
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            email = try container.decode(String.self, forKey: .email)
            gender = try container.decodeIfPresent(String.self, forKey: .gender)
            phone = try container.decodeIfPresent(String.self, forKey: .phone)

            let nameContainer = try container.nestedContainer(keyedBy: NameCodingKeys.self, forKey: .name)
            firstName = try nameContainer.decode(String.self, forKey: .firstName)
            lastName = try nameContainer.decode(String.self, forKey: .lastName)

            let pictureContainer = try container.nestedContainer(keyedBy: PictureCodingKeys.self, forKey: .picture)
            pictureLarge = try pictureContainer.decodeIfPresent(String.self, forKey: .large)
            pictureMedium = try pictureContainer.decodeIfPresent(String.self, forKey: .medium)
            pictureThumbnail = try pictureContainer.decodeIfPresent(String.self, forKey: .thumbnail)

            address = try? container.decodeIfPresent(Address.DTO.self, forKey: .address)
        }
    }
}

extension Contact {
    @nonobjc public class func fetchRequest(predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor] = []) -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact", predicate: predicate, sortDescriptors: sortDescriptors)
    }
}
