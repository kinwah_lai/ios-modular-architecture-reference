//
//  Feature1Coordinator.swift
//  Feature1
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import CoreData
import Foundation
import Moya

final public class Feature1Coordinator: BaseCoordinator {
    private (set) public var identifier: UUID = UUID()
    public var childCoordinators: [UUID: BaseCoordinator] = [:]
    public var finish: (BaseCoordinator) -> Void = { _ in }
    public let presenter: UINavigationController
    public let services: Services
    public let serviceProvider: MoyaProvider<RandomUserService>
    public let persistentContainer: NSPersistentContainer

    public init(services: Services, presenter: UINavigationController) {
        self.presenter = presenter
        self.services = services
        serviceProvider = MoyaProvider<RandomUserService>()
        persistentContainer = Feature1Coordinator.findAndLoadCoreDataModel()
    }

    static func findAndLoadCoreDataModel() -> NSPersistentContainer {
        let bundle = Bundle(identifier: "com.lai.Feature1")
        let modelUrl = bundle?.url(forResource: "CoreDataModels", withExtension: "momd")
        guard let url = modelUrl, let managedObjectModel = NSManagedObjectModel(contentsOf: url) else {
            fatalError()
        }
        let container = NSPersistentContainer(name: "CoreDataModels", managedObjectModel: managedObjectModel)
        container.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
        }
        return container
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    public func start() {
        let viewModel = Feature1ViewModel("Contact List", serviceProvider: serviceProvider, persistentContainer: persistentContainer)
        let feature = Feature1ViewController(nibName: nil, bundle: Bundle(for: type(of: self)))
        feature.delegate = self
        feature.viewModel = viewModel
        presenter.pushViewController(feature, animated: true)
    }
}

extension Feature1Coordinator: Feature1ViewControllerDelegate {
    func wantsToGoBack(at: Feature1ViewController) {
        finish(self)
    }
}
