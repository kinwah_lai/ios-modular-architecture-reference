//
//  SubtitleTableViewCell.swift
//  Contacts
//
//  Created by Darren Lai on 5/20/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import Core
import RxCocoa
import RxSwift
import UIKit

class SubtitleTableViewCell: UITableViewCell {
    private var disposeBag = DisposeBag()
    // swiftlint:disable implicitly_unwrapped_optional (because it injected from coordinator)
    var viewModel: ContactCellViewModel! {
        didSet {
            disposeBag.insert(
                // swiftlint:disable force_unwrapping
                viewModel.output.nameObservable.bind(to: textLabel!.rx.text),
                // swiftlint:disable force_unwrapping
                viewModel.output.emailObservable.bind(to: detailTextLabel!.rx.text)
            )
        }
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    convenience init(reuseIdentifier: String?) {
        self.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Common trap that subscription is not released
    /// https://medium.com/gett-engineering/disposing-rxswifts-memory-leaks-6ceb73162170
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
