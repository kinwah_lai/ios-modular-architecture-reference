//
//  ContactCellViewModel.swift
//  Contacts
//
//  Created by Darren Lai on 5/21/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import Core
import Foundation
import RxCocoa
import RxSwift

final class ContactCellViewModel: InputOutputViewModelType {
    let contact: Contact
    let input: ContactCellViewModel.Input
    let output: ContactCellViewModel.Output

    struct Input {
    }

    struct Output {
        let nameObservable: Observable<String>
        let emailObservable: Observable<String>
    }

    init(_ contact: Contact) {
        self.contact = contact
        input = Input()
        let nameObservable = Observable.just(contact.firstname + ", " + contact.lastname)
        let emailObservable = Observable.just(contact.email)
        output = Output(nameObservable: nameObservable, emailObservable: emailObservable)
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }
}
