//
//  Feature1ViewModel.swift
//  Feature1
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import CoreData
import Foundation
import Moya
import RxCocoa
import RxSwift

final class Feature1ViewModel: InputOutputViewModelType {
    enum AccessibilityIdentifier: String {
        case pageId = "contactlist-page"
    }
    private let serviceProvider: MoyaProvider<RandomUserService>
    private let persistentContainer: NSPersistentContainer
    private let disposeBag = DisposeBag()
    let input: Feature1ViewModel.Input
    let output: Feature1ViewModel.Output

    struct Input {}

    struct Output {
        let titleObservable: Observable<String>
        let contactListObservable: Observable<[ContactCellViewModel]>
    }

    init(_ title: String, serviceProvider: MoyaProvider<RandomUserService>, persistentContainer: NSPersistentContainer) {
        self.serviceProvider = serviceProvider
        self.persistentContainer = persistentContainer
        input = Input()
        let fetchRequest = Contact.fetchRequest(predicate: nil, sortDescriptors: [NSSortDescriptor(key: "email", ascending: true)])
        let contactsObservable: Observable<[Contact]> = persistentContainer.viewContext.rx.entities(fetchRequest: fetchRequest)
        let contactListCellObservable: Observable<[ContactCellViewModel]> = contactsObservable.mapMany(ContactCellViewModel.init).asObservable()
        output = Output(titleObservable: Observable.just(title), contactListObservable: contactListCellObservable)
        makeFetchContactObservable(serviceProvider: serviceProvider)
            .bind(onNext: persist)
            .disposed(by: disposeBag)
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    private func persist(dataTransferObjects: [Contact.DTO]) {
        let backgroundContext = persistentContainer.viewContext // need to notify other context if we using background context
        dataTransferObjects.forEach { object in
            try? Contact.findOrCreate(for: object, in: backgroundContext)
        }
        try? backgroundContext.save()
    }

    private func makeFetchContactObservable(serviceProvider: MoyaProvider<RandomUserService>) -> Observable<[Contact.DTO]> {
        return Observable.create { (observer) -> Disposable in
            let cancellable = serviceProvider.request(.randomSeededUser(6)) { result in
                switch result {
                case .success(let response):
                    do {
                        let codableResponse = try response.map(CodableResponse.self)
                        observer.onNext(codableResponse.results)
                        observer.onCompleted()
                    } catch {
                        observer.onError(RandomUserServiceError.parseContactFailed)
                    }
                case .failure:
                    observer.onError(RandomUserServiceError.remoteGetContactFailed)
                }
            }
            return Disposables.create {
                cancellable.cancel()
            }
            }.share()
    }
}
