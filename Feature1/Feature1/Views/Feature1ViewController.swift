//
//  Feature1ViewController.swift
//  Feature1
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import RxCocoa
import RxDataSources
import RxSwift
import RxSwiftExt
import UIKit

protocol Feature1ViewControllerDelegate: class {
    func wantsToGoBack(at: Feature1ViewController)
}

class Feature1ViewController: UIViewController {
    weak var delegate: Feature1ViewControllerDelegate?
    var viewModel: Feature1ViewModel!
    private let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: "Cell")
        setupAccessibilityIdentifier()
        disposeBag.insert(
            viewModel.output.titleObservable.bind(to: rx.title),
            viewModel.output.contactListObservable
                .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: SubtitleTableViewCell.self)) { _, element, cell in
                    cell.viewModel = element
            }
        )
    }

    override func didMove(toParent parent: UIViewController?) {
        if parent == nil {
            delegate?.wantsToGoBack(at: self)
        }
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    private func setupAccessibilityIdentifier() {
        view.accessibilityIdentifier = Feature1ViewModel.AccessibilityIdentifier.pageId.rawValue
    }
}
