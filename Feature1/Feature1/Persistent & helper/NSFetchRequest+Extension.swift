//
//  NSFetchRequest+Extension.swift
//  Contacts
//
//  Created by Darren Lai on 6/14/19.
//  Copyright © 2019 Lai. All rights reserved.
//

import CoreData
import Foundation

extension NSFetchRequest {
    @objc convenience init(entityName: String, predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor] = []) {
        self.init(entityName: entityName)
        self.predicate = predicate
        self.sortDescriptors = sortDescriptors
    }
}
