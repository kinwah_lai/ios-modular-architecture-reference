//
//  ContactService.swift
//  Feature1
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Foundation
import Moya

public enum RandomUserServiceError: Error {
    case imageDownloadFailed
    case parseContactFailed
    case remoteGetContactFailed
}

public enum RandomUserService {
    case randomSeededUser(Int)
    case portrait(String)
}

extension RandomUserService: TargetType {
    public var baseURL: URL {
        // swiftlint:disable force_unwrapping
        return URL(string: "https://randomuser.me")!
    }

    public var path: String {
        switch self {
        case .randomSeededUser:
            return "/api"
        case .portrait(let path):
            return path
        }
    }

    public var method: Moya.Method {
        return .get
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {
        switch self {
        case .randomSeededUser(let number):
            let parameters: [String: Any] = ["nat": "us", "exc": "login", "seed": "darren", "results": number]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .portrait:
            return .requestPlain
        }
    }

    public var headers: [String: String]? {
        return nil
    }
}
