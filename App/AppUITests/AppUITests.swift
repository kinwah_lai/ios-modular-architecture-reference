//
//  AppUITests.swift
//  AppUITests
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import XCTest

class AppUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDown() {
    }

    func testUIElementsExist() {
        let app = XCUIApplication()
        XCTAssertTrue(app.staticTexts["appid-label"].isHittable)
        XCTAssertTrue(app.staticTexts["appid-title-label"].isHittable)
        XCTAssertTrue(app.staticTexts["welcome-label"].isHittable)
        XCTAssertTrue(app.images["brand-logo"].isHittable)
        XCTAssertTrue(app.buttons["feature0-button"].isHittable)
        XCTAssertTrue(app.buttons["feature1-button"].isHittable)
    }

}
