//
//  MainViewController.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import RxCocoa
import RxSwift
import UIKit

protocol MainViewControllerDelegate: class {
    func presentFeature0()
    func pushFeature1()
}

class MainViewController: UIViewController {
    @IBOutlet weak var brandLogoTitleLabel: UILabel!
    @IBOutlet weak var brandLogo: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var appIdTitleLabel: UILabel!
    @IBOutlet weak var appIdLabel: UILabel!
    @IBOutlet weak var presentFeature0Button: UIButton!
    @IBOutlet weak var pushFeature1Button: UIButton!

    weak var delegate: MainViewControllerDelegate?
    var viewModel: MainViewModel!
    var disposeBag: DisposeBag = DisposeBag()

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAccessibilityIdentifier()
        disposeBag.insert(
            viewModel.output.appIdObservable.bind(to: appIdLabel.rx.text),
            viewModel.output.brandLogoObservable.bind(to: brandLogo.rx.image),
            viewModel.output.welcomeTitleObservable.bind(to: welcomeLabel.rx.text),
            viewModel.output.appIdTitleObservable.bind(to: appIdTitleLabel.rx.text),
            viewModel.output.brandLogoTitleObservable.bind(to: brandLogoTitleLabel.rx.text),
            presentFeature0Button.rx.tap.subscribe(onNext: { [unowned self] _ in
                self.delegate?.presentFeature0()
            }),
            pushFeature1Button.rx.tap.subscribe(onNext: { [unowned self] _ in
                self.delegate?.pushFeature1()
            })
        )
        [presentFeature0Button, pushFeature1Button].forEach { button in
            button?.layer.cornerRadius = 5
            button?.layer.borderWidth = 1
            button?.layer.borderColor = UIColor.lightGray.cgColor
        }
    }

    private func setupAccessibilityIdentifier() {
        brandLogoTitleLabel.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.brandLogoTitle.rawValue
        brandLogo.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.brandLogo.rawValue
        welcomeLabel.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.welcomeLabel.rawValue
        appIdTitleLabel.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.appIdTitleLabel.rawValue
        appIdLabel.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.appIdLabel.rawValue
        presentFeature0Button.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.feature0Button.rawValue
        pushFeature1Button.accessibilityIdentifier = MainViewModel.AccessibilityIdentifier.feature1Button.rawValue
    }

}
