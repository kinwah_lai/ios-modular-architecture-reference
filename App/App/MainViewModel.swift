//
//  MainViewModel.swift
//  App
//
//  Created by Darren Lai on 7/1/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import Foundation
import RxSwift

final class MainViewModel: InputOutputViewModelType {
    enum AccessibilityIdentifier: String {
        case brandLogo = "brand-logo"
        case brandLogoTitle = "brand-logo-title-label"
        case welcomeLabel = "welcome-label"
        case appIdTitleLabel = "appid-title-label"
        case appIdLabel = "appid-label"
        case feature0Button = "feature0-button"
        case feature1Button = "feature1-button"
    }

    let input: MainViewModel.Input
    let output: MainViewModel.Output

    struct Input {
    }

    struct Output {
        let appIdObservable: Observable<String?>
        let brandLogoObservable: Observable<UIImage?>
        let welcomeTitleObservable: Observable<String>
        let appIdTitleObservable: Observable<String>
        let brandLogoTitleObservable: Observable<String>
    }

    init() {
        input = Input()
        let appIdObs = Observable.just(Bundle.main.appId)
        let brandLogo = Observable.just(UIImage(named: "Brand Logo"))
        let welcomeTitle = Observable.just(NSLocalizedString("Welcome", comment: "Translate text: Welcome"))
        let appIdTitle = Observable.just(NSLocalizedString("App Id", comment: "Translate text: Application Id"))
        let brandLogoTitle = Observable.just(NSLocalizedString("Brand logo", comment: "Translate text: Brand logo"))
        output = Output(appIdObservable: appIdObs,
                        brandLogoObservable: brandLogo,
                        welcomeTitleObservable: welcomeTitle,
                        appIdTitleObservable: appIdTitle,
                        brandLogoTitleObservable: brandLogoTitle)
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }
}
