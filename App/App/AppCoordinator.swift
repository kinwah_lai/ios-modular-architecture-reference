//
//  AppCoordinator.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import UIKit

final class AppCoordinator: BaseCoordinator {
    private (set) var identifier: UUID = UUID()
    var childCoordinators: [UUID: BaseCoordinator] = [:]
    var finish: (BaseCoordinator) -> Void = { _ in }
    let presenter: UINavigationController
    let services: Services

    init(services: Services, presenter: UINavigationController) {
        self.presenter = presenter
        presenter.navigationBar.prefersLargeTitles = true
        self.services = services
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    func start() {
        let main = MainCoordinator(services: services, presenter: presenter)
        main.start()
        addChild(main)
    }

}
