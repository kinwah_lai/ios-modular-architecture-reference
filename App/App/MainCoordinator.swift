//
//  MainCoordinator.swift
//  App
//
//  Created by Darren Lai on 6/30/19.
//  Copyright © 2019 Darren Lai. All rights reserved.
//

import Core
import Feature0
import Feature1
import UIKit

final class MainCoordinator: BaseCoordinator {
    private (set) var identifier: UUID = UUID()
    var childCoordinators: [UUID: BaseCoordinator] = [:]
    var finish: (BaseCoordinator) -> Void = { _ in }
    let presenter: UINavigationController
    let services: Services

    init(services: Services, presenter: UINavigationController) {
        self.presenter = presenter
        self.services = services
    }

    deinit {
        log.debug("👉 deinit \(#file.lastPathComponent)")
    }

    func start() {
        let viewModel = MainViewModel()
        let mainVC = MainViewController(nibName: nil, bundle: nil)
        mainVC.viewModel = viewModel
        mainVC.delegate = self
        presenter.setViewControllers([mainVC], animated: true)
    }
}

extension MainCoordinator: MainViewControllerDelegate {
    func presentFeature0() {
        let feature0 = Feature0Coordinator(services: services, presenter: presenter)
        feature0.finish = { coordinator in
            self.removeChild(coordinator)
            log.debug("👉 poped \(coordinator.self)")
        }
        feature0.start()
        addChild(feature0)
    }

    func pushFeature1() {
        let feature1 = Feature1Coordinator(services: services, presenter: presenter)
        feature1.finish = { coordinator in
            self.removeChild(coordinator)
            log.debug("👉 poped \(coordinator.self)")
        }
        feature1.start()
        addChild(feature1)
    }
}
